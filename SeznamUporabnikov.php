<?php 
session_start();

include("PreverjanjeLogin/connection.php");
include("PreverjanjeLogin/CheckLogin.php");

	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <link rel="stylesheet" href="css/Stil.css">
        <link rel="stylesheet" href="css/SeznamUporabnikov.css">
        <title>Računalniške komponente</title>
        <!-- Favicon-->
        <link rel="icon" style="color: whitesmoke;"type="image/x-icon" href="assets/PCLogo.png"  />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />

        
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#!">Trgovina komponent</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li class="nav-item"><a class="nav-link active" aria-current="page" href="#!">Home</a></li>
                        <li class="nav-item"><a class="nav-link" href="#!">About</a></li>
                        <li id= "SeznamUporabnikov" class="nav-item"><a class="nav-link" href="SeznamUporabnikov.php">Seznam Uporabnikov</a></li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Shop</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#!">All Products</a></li>
                                <li><hr class="dropdown-divider" /></li>
                                <li><a class="dropdown-item" href="#!">Popular Items</a></li>
                                <li><a class="dropdown-item" href="#!">New Arrivals</a></li>
                            </ul>
                        </li>
                    </ul>
                    <form class="d-flex">
                        <button class="btn btn-outline-dark" type="submit">
                            <i class="bi-cart-fill me-1"></i>
                            Cart
                            <span class="badge bg-dark text-white ms-1 rounded-pill">0</span>
                        </button>
                        <button id="logout" class="btn btn-outline-dark"> <a href="PreverjanjeLogin/logout.php">Logout</a> </button>
                    </form>
                </div>
            </div>
        </nav>
        <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder">Seznam uporabnikov</h1>
                    <p class="lead fw-normal text-white-50 mb-0"></p>
                </div>
            </div>
        </header>
       
        <?php

$result = mysqli_query($con,"SELECT user_id, ime, email, user_level  FROM uporabnik");

echo "<table id= 'customers'>
  <tr>
    <th>user_id</th>
    <th>ime</th>
    <th>email</th>
    <th>user_level</th>
  </tr>";

while($row = mysqli_fetch_array($result))
{
    
echo "<tr>";
echo "<td>" . $row['user_id'] . "</td>";
echo "<td>" . $row['ime'] . "</td>";
echo "<td>" . $row['email'] . "</td>";
echo "<td>" . $row['user_level'] . "</td>";

echo "</tr>";
}
echo "</table>";

mysqli_close($con);
?>      
                           
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Vito Polenek 2021</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
